@extends('../layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($items as $item)
        <tr>
            <td>{{$item->id}}</td>
            <td>{{$item->title}}</td>
            <td>{{$item->created_at}}</td>
        </tr>
        <br>
        @endforeach
    </div>
</div>
@endsection
