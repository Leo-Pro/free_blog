@php
    /** @var \App\Models\BlogPost $item */
@endphp
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                @if ($item->is_published)
                    Опубликовано
                @else
                    Черновик
                @endif
            </div>
            <div class="card-body">
                <div class="card-title"></div>
                <div class="card-subtitle mb-2 text-muted"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"><a href="#maindata" data-toggle="tab" role="tab" class="nav-link active">Основные данные</a></li>
                    <li class="nav-item"><a href="#adddata" data-toggle="tab" role="tab" class="nav-link">Доп. данные</a></li>
                </ul>
                <br>
                <div class="tab-content">
                    <div class="tab-pane active" id="maindata" role="tabpanel">
                        <div class="form-group">
                            <label for="title">Заголовок</label>
                            <input type="text" id="title" name="title" class="form-control" minlength="3" value="{{ $item->title }}"required>
                        </div>
                        <div class="form-group">
                            <label for="content_raw">Статья</label>
                            <textarea name="content_raw" id="content_raw" rows="20" class="form-control">{{ old('content_raw', $item->content_raw) }}</textarea>
                        </div>
                    </div>
                    <div class="tab-pane" id="adddata" role="tabpanel">
                        <div class="form-group">
                            <label for="category_id">Категория</label>
                            <select name="category_id" id="category_id" class="form-control" placeholder="выберете категорию" required>
                                @foreach ($categoryList as $categoryOption)
                                <option value="{{ $categoryOption->id }}"
                                    @if ($categoryOption->id == $item->category_id)
                                        selected
                                    @endif>
                                    {{ $categoryOption->id_title }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="slug">Индетификатор</label>
                            <input type="text" id="slug" name="slug" class="form-control" value="{{ $item->slug }}">
                        </div>
                        <div class="form-group">
                            <label for="excert">Выдержка</label>
                            <textarea name="excerpt" id="excerpt" rows="3" class="form-control">{{ old('excerpt', $item->excerpt) }}</textarea>
                        </div>
                        <div class="form-check">
                            <input type="hidden" name="is_published" value="0" class="form-check-input">
                            <input type="checkbox" name="is_published" class="form-check-input" value="1" @if ($item->is_published) checked="checked" @endif>
                            <label for="is_published" class="form-check-label">Опубликовано</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
