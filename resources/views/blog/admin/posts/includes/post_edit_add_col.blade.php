<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <button class="btn btn-primary" type="submit">Сохранить</button>
            </div>
        </div>
    </div>
</div><br>
@if ($item->exists)
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">ID: {{ $item->id }}</div>
        </div>
    </div>
</div><br>
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Создано</label>
                    <input type="text" class="form-control" value="{{ $item->created_at }}" disabled>
                </div>
                <div class="form-group">
                    <label for="title">Изменено</label>
                    <input type="text" class="form-control" value="{{ $item->updated_at }}" disabled>
                </div>
                <div class="form-group">
                    <label for="title">Опубликовано</label>
                    <input type="text" class="form-control" value="{{ $item->published_at }}" disabled>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
